package org.deepedit.beserver;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.text.NumberFormat;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ReadDB extends HttpServlet {

    private Connection mylink;
    private static final String USUARIO_VALIDO = "uservalid";
    private static final String TEMP_DB = "MDE.mdb"; //Default DeepEdit Installation
    private static final String CONFIG_FILE = "config.txt";
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //
        String usuario;
        String tipoapuesta = "";
        String pass;
        String protocolo;
        String host;
        String puerto;
        String accion;
        String dbnombre;
        String bidtype;
        String strRutaBD = TEMP_DB;
        int nHoraIni = 12;
        int nHoraEnd = 16;

        String messageLine1 = "";
        String messageLine2 = "";
        String usuariovalido = "";
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        accion = request.getParameter("accion");
        dbnombre = request.getParameter("dbnombre");
        bidtype = request.getParameter("bidtype");
        String ipAddress = request.getRemoteAddr();

        //0--Data read needed:
        usuario = request.getParameter("UID");
        pass = request.getParameter("pass");
        protocolo = request.getParameter("protocolo");
        host = request.getParameter("host");
        puerto = request.getParameter("puerto");

        boolean bIsError = false;
        String strErrorTxt = "";
        int resinsert = 0;

        //Read Configuration File:
        try {
            
            // Open the file that is the first
            ServletContext context = getServletContext();
            String path = context.getRealPath(request.getContextPath());
            //System.out.println("Context path: " + path);
            FileInputStream fstream = new FileInputStream(path + "/" + CONFIG_FILE);
            // Get the object of DataInputStream
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            //Read File Line By Line
            while ((strLine = br.readLine()) != null) {
                String strConfigLine[] = strLine.split(" ");

                if (strConfigLine.length >= 2) {
                    String strParameterConfig = strConfigLine[0];
                    String strValueConfig = "";
                    for (int ii = 1; ii < strConfigLine.length; ii++) {
                        strValueConfig += strConfigLine[ii];
                    }

                    if (strParameterConfig.equals("UBICACION_BD")) {
                        strRutaBD = strValueConfig;
                    } else if (strParameterConfig.equals("HORA_INICIO")) {
                        nHoraIni = Integer.parseInt(strValueConfig);
                    } else if (strParameterConfig.equals("HORA_FIN")) {
                        nHoraEnd = Integer.parseInt(strValueConfig);
                    } else {
                        //Here you can add more options:
                    }

                }
            }
            //Close the input stream
            in.close();
        } catch (IOException e) {//Catch exception if any
            bIsError = true;
            out.print("error");
            out.print(e.getMessage());
            System.out.println("Error: " + e.getMessage());
            strErrorTxt = e.getMessage();
        } catch (NumberFormatException e) {
            bIsError = true;
            out.print("error");
            out.print("Fatal error parsing numeric info in config file: " + e.getMessage());
            System.out.println("Error: " + e.getMessage());
            strErrorTxt = e.getMessage();
        }

        try {
//                Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
        } catch (ClassNotFoundException ex) {
            bIsError = true;
            out.print("error");
            out.print(ex.getMessage());
            System.out.println("SQLException: " + ex.getMessage());
            strErrorTxt = ex.getMessage();
        }

//        String sURL = "jdbc:odbc:Driver={Microsoft Access Driver (*.mdb)};DBQ=";
//            sURL += strRutaBD + ";DriverID=22;READONLY=true}";
        String sURL = "jdbc:ucanaccess://" + strRutaBD + ";immediatelyreleaseresources=true";

//******************
//Validation process
//******************
        try {
            mylink = DriverManager.getConnection(sURL, "", "");
            //1--Autentification query:
            String consult = "SELECT * FROM usergeneral WHERE loginname='" + usuario + "'";
            Statement stm = mylink.createStatement();//ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet res = stm.executeQuery(consult);
            while (res.next()) {
                String passdec = res.getString("pass");
                tipoapuesta = res.getString("bidtype");
                if (passdec.equals(pass) && tipoapuesta.equals(bidtype)) {
                    usuariovalido = USUARIO_VALIDO;
                }
            }
        } catch (SQLException ex) {
            bIsError = true;
            out.print("error");
            out.print("SQLException: " + ex.getMessage());
            System.out.println("SQLException: " + ex.getMessage());
            strErrorTxt = ex.getMessage();
        }

        if (usuariovalido.equals(USUARIO_VALIDO)) {
            if (accion.equals("actualizar")) {
                if (validarhr(nHoraIni, nHoraEnd)) {

//******************
//DELETE EXISTING "OFERTA" (all-24-hours)
//******************
                    int resdelete = 0;
                    //0--Data delete needed:
                    usuario = request.getParameter("UID");
                    //1--Not needed
                    //2--Not needed
                    //3--Delete Query:
                    try {
                        int k;
                        String consult = "";
                        for (k = 0; k < 24; k++) {
                            if (bidtype.equals("compra")) {
                                consult = "DELETE FROM OAHORA" + (k + 1) + " WHERE COD_UA='" + usuario + "'";
                            } else if (bidtype.equals("venta")) {
                                consult = "DELETE FROM OVHORA" + (k + 1) + " WHERE COD_UP='" + usuario + "'";
                            } else {
                                System.out.println("DataBase field error!");
                            }
                            Statement stm = mylink.createStatement();
                            resdelete = resdelete + stm.executeUpdate(consult);
                        }

                    } catch (SQLException ex) {
                        bIsError = true;
                        out.print("error");
                        out.print(ex.getMessage());
                        System.out.println("SQLException: " + ex.getMessage());
                        strErrorTxt = ex.getMessage();
                        //JOptionPane.showMessageDialog(null, "SQLExcepception1: "+ex.getMessage(),"ERROR", JOptionPane.ERROR_MESSAGE);
                    }

//******************
//INSERT NEW USERID "OFERTA" (all-24-hours)
//******************
                    //0--Data insert needed:
                    String offerpoint = request.getParameter("offerpoint");
                    String offerhour[] = offerpoint.split("//");
                    //1--Not needed
                    //2--Not needed
                    //3--Insert Query:
                    String sHour = "";
                    String sTranche = "";
                    String sValue = "";
                    try {
                        String consult = "";
                        for (int ix = 0; ix < offerhour.length; ix++) {
                            String opoint[] = offerhour[ix].split("::");
                            if (opoint.length == 4) {
                                sHour = opoint[0];
                                sTranche = opoint[1];
                                if (bidtype.equals("compra")) {
                                    consult = "INSERT INTO OAHORA" + opoint[0] + " VALUES ('" + usuario + "','" + opoint[1] + "','" + formatdoubledb(opoint[2]) + "','" + formatdoubledb(opoint[3]) + "')";
                                } else if (bidtype.equals("venta")) {
                                    consult = "INSERT INTO OVHORA" + opoint[0] + " VALUES ('" + usuario + "','" + opoint[1] + "','" + formatdoubledb(opoint[2]) + "','" + formatdoubledb(opoint[3]) + "')";
                                } else {
                                    System.out.println("DataBase field error!");
                                }
                                Statement stm = mylink.createStatement();
                                resinsert = resinsert + stm.executeUpdate(consult);
                            }
                        }
                        messageLine1 = "informacion";  //we can catch here resdelete and resinsert
                        messageLine2 = "Base de datos ha sido actualizada";
                    } catch (SQLException ex) {
                        bIsError = true;
                        out.print("error");
                        out.print(ex.getMessage());
                        System.out.println("SQLException: " + ex.getMessage());
                        strErrorTxt = ex.getMessage();
                    } catch (NumberFormatException ex) {
                        messageLine1 = "error";
                        messageLine2 = "Couldn't parse offer point for '" + usuario + "' hour [ " + sHour + "] tranche [" + sTranche + "]. " + ex.getMessage();
                    }
                } else {
                    messageLine1 = "error";
                    messageLine2 = "Acceso denegado. Proceso de admision de ofertas cerrado";
                }

                //4--Write small log to console
                Calendar rightNow = Calendar.getInstance();
                System.out.println("Submission from:" + usuario);
                System.out.println(rightNow.getTime());

                //5--Write log to database:
                try {
                    String consult = "";
                    if (messageLine1.equals("informacion")) {
                        if (bidtype.equals("compra")) {
                            consult = "INSERT INTO logEvent (idUser, numData, ipAddress, action, type) VALUES ('" + usuario + "','" + resinsert + "','" + ipAddress + "','compra','insert')";
                        } else if (bidtype.equals("venta")) {
                            consult = "INSERT INTO logEvent (idUser, numData, ipAddress, action, type) VALUES ('" + usuario + "','" + resinsert + "','" + ipAddress + "','venta','insert')";
                        } else {
                            System.out.println("DataBase field error!");
                        }
                        Statement stm = mylink.createStatement();
                        stm.executeUpdate(consult);
                    }
                } catch (SQLException ex) {
                    bIsError = true;
                    out.print("error");
                    out.print(ex.getMessage());
                    System.out.println("SQLException: " + ex.getMessage());
                    strErrorTxt = ex.getMessage();
                }

                //Send oferta stored in DB to applet:
            } else if (accion.equals("cargardb")) {

                //0--Data insert needed:
                int hora = 0;
                int contblock = 0;
                String tramo = "";
                String precio = "";
                String energia = "";
                String offerhour = "";
                //1--Not needed
                //2--Not needed
                //3--Insert Query:
                try {
                    String consult = "";
                    for (int ix = 0; ix < 24; ix++) {
                        if (bidtype.equals("compra")) {
                            consult = "SELECT * FROM OAHORA" + (ix + 1) + " WHERE COD_UA='" + usuario + "'";
                        } else if (bidtype.equals("venta")) {
                            consult = "SELECT * FROM OVHORA" + (ix + 1) + " WHERE COD_UP='" + usuario + "'";
                        } else {
                            System.out.println("DataBase field error!");
                        }
                        Statement stm = mylink.createStatement();
                        ResultSet res = stm.executeQuery(consult);
                        while (res.next()) {
                            hora = ix + 1;
                            tramo = res.getString("TRAMO");
                            precio = res.getString("PRECIO");
                            energia = res.getString("ENERGIA");

                            if (contblock == 0) {
                                offerhour = hora + "::" + tramo + "::" + precio + "::" + energia;
                            } else {
                                offerhour = offerhour + "//" + hora + "::" + tramo + "::" + precio + "::" + energia;
                            }
                            contblock++;
                        }
                    }

                } catch (SQLException ex) {
                    bIsError = true;
                    out.println("error");
                    out.println(ex.getMessage());
                    System.out.println("SQLException: " + ex.getMessage());
                    strErrorTxt = ex.getMessage();
                    //JOptionPane.showMessageDialog(null, "SQLExcepception2: "+ex.getMessage(),"ERROR", JOptionPane.ERROR_MESSAGE);
                }

                messageLine1 = "informacion";  //we can catch here resdelete and resinsert
                messageLine2 = offerhour;

            }

        } else {
            if (bIsError) {
                messageLine1 = "error";
                messageLine2 = strErrorTxt;
            } else {
                messageLine1 = "error";
                messageLine2 = "Usuario No Valido.";
            }
        }

//******************
//Close DB conection:
//******************
        try {
            mylink.close();
        } catch (SQLException ex) {
            bIsError = true;
            out.println("error");
            out.println(ex.getMessage());
            System.out.println("SQLException: " + ex.getMessage());
        }

        if (!bIsError) {
            out.println(messageLine1);
            out.println(messageLine2);
        }
    }

    /**
     * No Changes allow to DB from 12:00pm to 4pm (validation method):
     *
     * @param iniserveroff zero-based closing hour of day
     * @param endserveroff zero-based re-openning hour of day
     * @return true if chances were sucessfully commmitted
     */
    public boolean validarhr(int iniserveroff, int endserveroff) {
        Calendar rightNow = Calendar.getInstance();
        int actualhour = rightNow.get(Calendar.HOUR_OF_DAY);
        return (actualhour < iniserveroff) || (actualhour >= endserveroff);
    }

    /**
     * Format double with regional. Unsure
     * @param dg string representation of value
     * @return double formated valur
     */
    public String formatdoubledb(String dg) {
        Double gap = Double.parseDouble(dg);
        String amountOut;
        NumberFormat numberFormatter = NumberFormat.getInstance();
        numberFormatter.setMaximumFractionDigits(3);
        numberFormatter.setGroupingUsed(false);
        amountOut = numberFormatter.format(gap);
        return amountOut;
    }
	
	
}